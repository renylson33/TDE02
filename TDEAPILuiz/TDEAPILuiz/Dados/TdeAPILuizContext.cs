﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TDEAPILuiz.Models;

namespace TDEAPILuiz.Dados
{
    public class TdeAPILuizContext : DbContext
    {

        public TdeAPILuizContext(DbContextOptions<TdeAPILuizContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FuncionarioOcorrencia>().HasKey(ac => new { ac.IdFuncionario, ac.IdOcorrencia });
        }


        public DbSet<Funcionario> Funcionarios { get; set; }

        public DbSet<Departamento> Departamentos { get; set; }

        public DbSet<Ocorrencia> Ocorrencias { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<FuncionarioOcorrencia> FuncionarioOcorrencias { get; set; }


    }
}
