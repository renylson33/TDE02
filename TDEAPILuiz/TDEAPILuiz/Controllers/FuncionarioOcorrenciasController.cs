﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TDEAPILuiz.Dados;
using TDEAPILuiz.Models;

namespace TDEAPILuiz.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionarioOcorrenciasController : ControllerBase
    {
        private readonly TdeAPILuizContext _context;

        public FuncionarioOcorrenciasController(TdeAPILuizContext context)
        {
            _context = context;
        }

        // GET: api/FuncionarioOcorrencias
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FuncionarioOcorrencia>>> GetFuncionarioOcorrencias()
        {
            return await _context.FuncionarioOcorrencias.ToListAsync();
        }

        // GET: api/FuncionarioOcorrencias/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FuncionarioOcorrencia>> GetFuncionarioOcorrencia(int id)
        {
            var funcionarioOcorrencia = await _context.FuncionarioOcorrencias.FindAsync(id);

            if (funcionarioOcorrencia == null)
            {
                return NotFound();
            }

            return funcionarioOcorrencia;
        }

        // PUT: api/FuncionarioOcorrencias/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFuncionarioOcorrencia(int id, FuncionarioOcorrencia funcionarioOcorrencia)
        {
            if (id != funcionarioOcorrencia.IdFuncionario)
            {
                return BadRequest();
            }

            _context.Entry(funcionarioOcorrencia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionarioOcorrenciaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FuncionarioOcorrencias
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FuncionarioOcorrencia>> PostFuncionarioOcorrencia(FuncionarioOcorrencia funcionarioOcorrencia)
        {
            _context.FuncionarioOcorrencias.Add(funcionarioOcorrencia);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FuncionarioOcorrenciaExists(funcionarioOcorrencia.IdFuncionario))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFuncionarioOcorrencia", new { id = funcionarioOcorrencia.IdFuncionario }, funcionarioOcorrencia);
        }

        // DELETE: api/FuncionarioOcorrencias/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FuncionarioOcorrencia>> DeleteFuncionarioOcorrencia(int id)
        {
            var funcionarioOcorrencia = await _context.FuncionarioOcorrencias.FindAsync(id);
            if (funcionarioOcorrencia == null)
            {
                return NotFound();
            }

            _context.FuncionarioOcorrencias.Remove(funcionarioOcorrencia);
            await _context.SaveChangesAsync();

            return funcionarioOcorrencia;
        }

        private bool FuncionarioOcorrenciaExists(int id)
        {
            return _context.FuncionarioOcorrencias.Any(e => e.IdFuncionario == id);
        }
    }
}
