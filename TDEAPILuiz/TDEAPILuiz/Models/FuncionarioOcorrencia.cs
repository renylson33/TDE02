﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDEAPILuiz.Models
{
    public class FuncionarioOcorrencia
    {

        public int IdFuncionario { get; set; }

        public int IdOcorrencia { get; set; }   


        public Funcionario Funcionario { get; set; }

        public Ocorrencia Ocorrencia { get; set; }

    }
}
